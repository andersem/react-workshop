class: center, middle, inverse

# REACT-workshop

---

# Først

---
class: center, middle

# Bidragsmedlem? 
Send mail! ([yammer](https://www.yammer.com/visma.com/#/groups/2783701)).

---
class: center, middle, inverse

# Kurs i testdreven javascript-utvikling

---

# Hva er React?

* V i MVC 
* Bibliotek, ikke rammeverk
* Virtuell DOM

---
class: center, middle, inverse

# Let's start 

---

# Første skritt

```html
<!-- index.html -->
<html>
<head>
    <title>Hello React</title>
    <script src="http://fb.me/react-0.11.1.js"></script>
    <script src="http://fb.me/JSXTransformer-0.11.1.js"></script>
</head>
<body>
<div id="content"></div>
</body>
</html>
```

---

# URLer

* http://vsin-sc-osl-076:8000/pres.html
* http://vsin-sc-osl-076:3000/
---

# Første skritt

```html
<!-- index.html -->
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Kontakter</title>
    <script src="http://fb.me/react-0.11.1.js"></script>
    <script src="http://fb.me/JSXTransformer-0.11.1.js"></script>
</head>
<body>
<div id="content"></div>

    <script type="text/jsx">
    /** @jsx React.DOM */
    </script>

</body>
</html>
```

---

# ContactBox

```javascript
var ContactBox = React.createClass({
    render: function(){
        return (
            <div className="contactContainer">
                Her kommer en liste med kontakter!
            </div>
        )
    }
});

```

---

# Render

```javascript
var ContactBox = React.createClass({
    render: function(){
        return (
            <div className="contactContainer">
                Her kommer en liste med kontakter!
            </div>
        )
    }
});

React.renderComponent(<ContactBox />, document.getElementById('content'));

```

---

# ContactList

```javascript
var ContactList = React.createClass({
  render: function() {
    return (
      <div className="ContactList">
        Liste med kontakter
      </div>
    );
  }
});

```

---

# ContactForm

```javascript
var ContactForm = React.createClass({
  render: function() {
    return (
      <div className="ContactForm">
        Form for å poste kontakter
      </div>
    );
  }
});
```

---

# Forandre ContactBox

```javascript
var ContactBox = React.createClass({
    render: function(){
        return (
            <div className="contactBox">
                        <h1>Kontakter</h1>
                        <ContactList />
                        <ContactForm />
            </div>
        )
    }
});
```

---

class: center, middle, inverse

# F5

---

# Contact

```javascript
var Contact = React.createClass({
  render: function() {
    return (
      <div className="Contact">
        <h2 className="ContactName">
          {this.props.name}
        </h2>
        <p>
        {this.props.number}
        </p>
      </div>
    );
  }
});
```
---

# Contact

```javascript
var Contact = React.createClass({
  render: function() {
    return (
      <div className="Contact">
        <h2 className="ContactName">
          {this.props.name}
        </h2>
        <p>
        {this.props.number}
        </p>
      </div>
    );
  }
});
```

```javascript
var ContactList = React.createClass({
  render: function() {
    return (
      <div className="ContactList">
        <Contact name="Bruce Banner" number="41520477"/>
        <Contact name="Peter Parker" number="94934858" />
      </div>
    );
  }
});
```


---

# Få inn data utenifra

```javascript
var data = [
  {name: "Bruce Banner", number: "415204773"},
  {name: "Peter Parker", number: "94934858"}
];

```

```javascript
React.renderComponent(<ContactBox data={data}/>, document.getElementById('content'));
```

---

# Ta data inn i commentbox

```javascript
var ContactBox = React.createClass({
    render: function(){
        return (
            <div className="contactBox">
                        <h1>Kontakter</h1>
                        <ContactList data={this.props.data}/>
                        <ContactForm />
            </div>
        )
    }
});

```

---

# Forandre kontaktlisten

```javascript
var ContactList = React.createClass({
  render: function() {
    var ContactNodes = this.props.data.map(function (contact) {
      return (
        <Contact name={contact.name} number={contact.number} />
      );
    });
    return (
      <div className="ContactList">
        {ContactNodes}
      </div>
    );
  }
});
```

---

# Over http

```html
<script src="http://code.jquery.com/jquery-1.10.0.min.js"></script>
```

```javascript
React.renderComponent(<ContactBox url="http://vsin-sc-osl-076:3000/contacts/" />, document.getElementById('content'));
```
---
# Forandre contactbox

```javascript
var ContactBox = React.createClass({
  getInitialState: function() {
    return {data: []};
  },
    render: function(){
        return (
            <div className="contactBox">
                        <h1>Kontakter</h1>
                        <ContactList data={this.state.data}/>
                        <ContactForm />
            </div>
        )
    }
});
```


---

# GET fra server

```html
<script src="http://code.jquery.com/jquery-1.10.0.min.js"></script>
```

```javascript
var ContactBox = React.createClass({
...
componentDidMount: function() {
            $.ajax({
              url: this.props.url,
              dataType: 'json',
              success: function(data) {
                this.setState({data: data});
              }.bind(this),
              error: function(xhr, status, err) {
                console.error(this.props.url, status, err.toString());
              }.bind(this)
            });
            },
...

});

```


